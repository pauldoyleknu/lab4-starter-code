#Author: Paul Doyle
# Date: June 2017

from flask import Flask, Response, request
from json import dumps

app = Flask(__name__)

@app.route('/')
def index():
  return """

Available API endpoints:

POST /interest			addition of 2 numbers 
POST /pow			find the power of a number
POST /metric			convert from meters to inches
POST /max			find max number of two numbers
POST /min			find the power of a number

GET /functions		return a list of supported functions
"""

#
#
# This is the default location for the web server
# When users access the website, the front page will explain the 
# REST APIs that are accessible. 
# The first two examples have been completed, you will need to 
# fill out the code to support the remaining API calls
#
#

app = Flask(__name__)

#
# Example:
#
# curl -s -X GET -H 'Accept: application/json' https://web.134vm.nightsky.ie/functions
# {"operators": ["interest","pow", "metric", "max","min"]}
#
@app.route('/functions', methods=['GET'])
def operators():
  resp = dumps({ 
    "functions":["interest","pow","metric","max","min"]
  })
  return Response(response=resp, mimetype="application/json")

#
# Example:
#
# curl -s -X POST -H 'Accept: application/json' -H 'Content-type: application/json' https://web.134vm.nightsky.ie/interest -d '{"principle":100,"years":20,"ratepercent":5}'
# {"result": 30}
#
@app.route('/interest', methods=['POST'])
def interest():
  args = request.get_json()
  balance = int(args['principle'])
  rate= float(args['ratepercent'])/100
  term = int(args['years'])

  print args
  try:
    while(term > 0):
	balance = balance * (1 + rate)
	term = term -1

  except Exception:
    pass

  resp = dumps({
    "result": balance
  })
  
  return Response(response=resp, mimetype="application/json")


#
# Example:
#   
# curl -s -X POST -H 'Accept: application/json' -H 'Content-type: application/json' https://web.134vm.nightsky.ie/min -d '{"n1":100,"n2":20}'
# {"result": 100}
#
@app.route('/min', methods=['POST'])
def min():
  resp = ''

  return Response(response=resp, mimetype="application/json")









if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=5000)
