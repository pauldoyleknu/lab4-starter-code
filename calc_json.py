from flask import Flask, Response, request
from json import dumps

# Author: Paul Doyle/Brian Gillespie June 2017
# Use JSON arguments to send and receive data to the
# web server for performing calculations.
# Parameters are sent using JSON format and the output
# is in JSON format. Use CURL to access this service, not a browser


app = Flask(__name__)

#
# Example:
#
# curl -s -X GET -H 'Accept: application/json' https://web.134vm.nightsky.ie/operators
# {"operators": ["+", "-", "x", "/"]}
#
@app.route('/operators', methods=['GET'])
def operators():
  resp = dumps({ 
    "operators":["+","-","x","/"]
  })
  return Response(response=resp, mimetype="application/json")

#
# Example:
#
# curl -s -X POST -H 'Accept: application/json' -H 'Content-type: application/json' https://web.134vm.nightsky.ie/calculate -d '{"n1":10,"n2":20,"operator":"+"}'
# {"result": 30}
#
@app.route('/calculate', methods=['POST'])
def calculate():
  result = 0
  args = request.get_json()
  print args
  try:
    if args['operator'] == u'+':
      result = int(args['n1']) + int(args['n2'])
    elif args['operator'] == u'-':
      result = int(args['n1']) - int(args['n2'])
    elif args['operator'] == u'x':
      result = int(args['n1']) * int(args['n2'])
    else:
      result = int(args['n1']) / int(args['n2'])
  except Exception:
    pass

  resp = dumps({
    "result": result
  })
  
  return Response(response=resp, mimetype="application/json")

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=5000)
