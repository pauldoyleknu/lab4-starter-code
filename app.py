from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World! - This is an update"

@app.route('/knu')
def knu():
    return "Welcome to KNU"

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
