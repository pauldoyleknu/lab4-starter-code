from flask import Flask, request
app = Flask(__name__)

# Author: Paul Doyle/Brian Gillespie June 2017
# Use a web form to POST data to the form and to return the result of a calculation

template = '''
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style>body { margin: 20px; } </style>
</head>
<body>
  <form action="/calculate" method="post" class="form-inline">
    <input type="number" class="form-control" name="n1" value="%d" autofocus>
    <select name="operator" class="form-control">
      <option>+</option>
      <option>&#247;</option>
    </select>
    <input type="number" class="form-control" name="n2" value="%d">
    <button type="submit" class="btn btn-primary">Calculate</button>
    &rarr;
    <input type="number" class="form-control" value="%d" readonly>
  </form>
</body>
</html>
'''

@app.route('/')
def index():
  return template % (0, 0, 0)

@app.route('/calculate', methods=['POST'])
def calculate():
  result = 0
  args = request.form
  try:
    if args['operator'] == u'+':
      result = int(args['n1']) + int(args['n2'])
    else:
      result = int(args['n1']) / int(args['n2'])
  except Exception:
    pass

  return template % (int(args['n1']), int(args['n2']), result)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=5000)
